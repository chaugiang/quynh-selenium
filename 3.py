from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

userName = "quynhdt6@fsoft.com.vn"
userPass = "13011996"

driver = webdriver.Firefox()
driver.get("https://pico.vn/login")

inputEmail = driver.find_element_by_css_selector('#Home_ContentPlaceHolder_tb_Email')
assert "E-mail" in inputEmail.get_property("placeholder")

inputPassword = driver.find_element_by_css_selector('#Home_ContentPlaceHolder_tb_Pasw')
assert "Mật khẩu" in inputPassword.get_property("placeholder")

buttonLogin = driver.find_element_by_id("Home_ContentPlaceHolder_bt_Submit")
assert "Đăng nhập" in buttonLogin.get_property("value") 

# Login success
inputEmail.send_keys(userName)
inputPassword.send_keys(userPass)
buttonLogin.click()

time.sleep(1)
assert "Xin chào" in driver.page_source
assert "Do Quynh" in driver.page_source

# Logout
driver.get("https://pico.vn/logout")
driver.get("https://pico.vn/login")

# Login Failed: empty input

driver.find_element_by_id("Home_ContentPlaceHolder_bt_Submit").click()
assert "Đăng nhập sai hoặc Tài khoản chưa được kích hoạt!" in driver.page_source

# Login Failed: wrong email, correct password

driver.find_element_by_css_selector('#Home_ContentPlaceHolder_tb_Pasw').send_keys(userPass)
driver.find_element_by_id("Home_ContentPlaceHolder_bt_Submit").click()
assert "Đăng nhập sai hoặc Tài khoản chưa được kích hoạt!" in driver.page_source

# Login Failed: wrong password, correct email

inputEmail = driver.find_element_by_css_selector('#Home_ContentPlaceHolder_tb_Email').send_keys(userName)
driver.find_element_by_id("Home_ContentPlaceHolder_bt_Submit").click()
assert "Đăng nhập sai hoặc Tài khoản chưa được kích hoạt!" in driver.page_source