from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import UnexpectedAlertPresentException
from selenium.common.exceptions import NoAlertPresentException

import time
import unittest

# Declare global variable
PICO_LINK = "https://pico.vn"
PICO_LINK_LOGIN = "https://pico.vn/login"
PICO_LINK_TO_CART = "https://pico.vn/cart"
LINK_ITEM_POFOKO = "https://pico.vn/36839/balo-may-tinh-pofoko-qad-15-mau-den.html"
LINK_ITEM_DELL_BAG = "https://pico.vn/36740/balo-dell-cao-cap--essential.html"

USER_NAME = 'quynhdt6@fsoft.com.vn'
USER_PASS = "13011996"

ADD_TO_CART_BTN = "a.action-button:nth-child(2)"
COUNT_ITEM_ELEMENT = "#count_shopping_cart"
FIRST_ITEM_IN_CART = ".orderinfo-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)"
SECOND_ITEM_IN_CART = ".orderinfo-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > a:nth-child(1)"
DEL_BTN_IN_CART = ".orderinfo-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3) > p:nth-child(2) > a:nth-child(2)"
TOTAL_COST = ".orderinfo-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > strong:nth-child(1)"
INCREMENT_BTN = ".icon-up-dir"
# This code below for testing search function
# class PicoTestSearch(unittest.TestCase):
  # def setUp(self):
  #   self.driver = webdriver.Firefox()
  #   self.driver.get(PICO_LINK)
  #   self.popupElement = self.driver.find_element_by_css_selector('.box-promotion-close')
  #   self.popupElement.click()
  #   self.searchBox = self.driver.find_element_by_css_selector('#SearchKeyword')

  # def test_title(self):
  #   driver = self.driver
  #   self.assertIn("Siêu thị Điện máy Pico", driver.title)

  # def test_search_has_result(self):
  #   driver = self.driver
  #   searchBox = self.searchBox
  #   queryTextFound = ' balo'
  #   for character in queryTextFound:
  #     searchBox.send_keys(character)
  #     time.sleep(0.5)
  #   searchBox.send_keys(Keys.LEFT)
  #   searchBox.send_keys(Keys.SPACE)
  #   searchBox.send_keys(Keys.ARROW_DOWN)
  #   time.sleep(5)
  #   listItem = driver.find_element_by_id('SearchAutoComplete')
  #   assert listItem.is_displayed() is True
  #   searchBox.send_keys(Keys.RETURN)
  #   time.sleep(3)
  #   assert "Balo Dell" in driver.page_source
  #   assert "Từ khóa bạn tìm kiếm không có kết quả nào phù hợp" not in driver.page_source

  # def test_search_no_result(self):
  #   driver = self.driver
  #   searchBox = self.searchBox
  #   queryTextNotFound = 'khong ton tai'
  #   for character in queryTextNotFound:
  #     searchBox.send_keys(character)
  #     time.sleep(0.1)
  #   searchBox.send_keys(Keys.RETURN)
  #   time.sleep(2)
  #   assert "Từ khóa bạn tìm kiếm không có kết quả nào phù hợp" in driver.page_source

  # def tearDown(self):
  #   self.driver.close()

# This code below for testing login function
# class PicoTestLogin(unittest.TestCase):
#   def setUp(self):
#     self.driver = webdriver.Firefox()
#     self.driver.get(PICO_LINK_LOGIN)
#     self.input_email = self.driver.find_element_by_css_selector('#Home_ContentPlaceHolder_tb_Email')
#     self.input_password = self.driver.find_element_by_css_selector('#Home_ContentPlaceHolder_tb_Pasw')
#     self.button_login = self.driver.find_element_by_id("Home_ContentPlaceHolder_bt_Submit")

#   def test_login_screen(self):
#     input_email = self.input_email
#     input_password = self.input_password
#     button_login = self.button_login
#     assert "E-mail" in input_email.get_property("placeholder")
#     assert "Mật khẩu" in input_password.get_property("placeholder")
#     assert "Đăng nhập" in button_login.get_property("value")

#   def test_login_success(self):
#     driver = self.driver
#     input_email = self.input_email
#     input_password = self.input_password
#     button_login = self.button_login
#     input_email.send_keys(USER_NAME)
#     input_password.send_keys(USER_PASS)
#     button_login.click()
#     time.sleep(3)
#     assert "Xin chào" in driver.page_source
#     assert "Do Quynh" in driver.page_source

#   def test_login_wrong_both_user_name_and_pass(self):
#     driver = self.driver
#     input_email = self.input_email
#     input_password = self.input_password
#     button_login = self.button_login
#     input_email.send_keys('♥!♥')
#     input_password.send_keys('1')
#     button_login.click()
#     time.sleep(1)
#     assert "Đăng nhập sai hoặc Tài khoản chưa được kích hoạt!" in driver.page_source

#   def test_login_wrong_user_name(self):
#     driver = self.driver
#     input_email = self.input_email
#     input_password = self.input_password
#     button_login = self.button_login
#     input_email.send_keys('♥!♥')
#     input_password.send_keys(USER_PASS)
#     button_login.click()
#     time.sleep(1)
#     assert "Đăng nhập sai hoặc Tài khoản chưa được kích hoạt!" in driver.page_source

#   def test_login_wrong_pass(self):
#     driver = self.driver
#     input_email = self.input_email
#     input_password = self.input_password
#     button_login = self.button_login
#     input_email.send_keys(USER_NAME)
#     input_password.send_keys('wrong_pass')
#     button_login.click()
#     time.sleep(1)
#     assert "Đăng nhập sai hoặc Tài khoản chưa được kích hoạt!" in driver.page_source

#   def tearDown(self):
#     self.driver.close()

# This code below for testing order function
class PythonOrgSearch(unittest.TestCase):
  def setUp(self):
    self.driver = webdriver.Firefox()

  def test_order_item_pico(self):
    driver = self.driver
    driver.get(LINK_ITEM_POFOKO)
    # Find item pofoko then add it to card
    driver.find_element_by_css_selector(ADD_TO_CART_BTN).click()
    time.sleep(3)
    Alert(driver).accept()
    time.sleep(5)
    # Check item count from 0 -> 1
    assert "1" in driver.find_element_by_css_selector(COUNT_ITEM_ELEMENT).get_attribute('innerHTML')

    # Go to cart
    driver.get(PICO_LINK_TO_CART)
    time.sleep(3)
    pofoko_item = driver.find_element_by_css_selector(FIRST_ITEM_IN_CART)
    # Check item in cart is exact item that we just added 
    assert "Balo Máy Tính Pofoko QAD15 Đen" in pofoko_item.get_attribute("innerHTML")

    # Go to another item and add it to cart
    driver.get(LINK_ITEM_DELL_BAG)
    driver.find_element_by_css_selector(ADD_TO_CART_BTN).click()
    time.sleep(3)
    Alert(driver).accept()
    time.sleep(3)
    # Check item count from 1 -> 2
    assert "2" in driver.find_element_by_css_selector(COUNT_ITEM_ELEMENT).get_attribute('innerHTML')

    # Go to cart
    driver.get(PICO_LINK_TO_CART)
    time.sleep(3)
    dell_item = driver.find_element_by_css_selector(SECOND_ITEM_IN_CART)
    # Check item dell in cart
    assert "Balo Dell Cao Cấp" in dell_item.get_attribute("innerHTML")
    # Double check that item pofoko still in cart
    pofoko_item = driver.find_element_by_css_selector(FIRST_ITEM_IN_CART)
    assert "Balo Máy Tính Pofoko QAD15 Đen" in pofoko_item.get_attribute("innerHTML")

    # Delete item pofoko from cart
    driver.find_element_by_css_selector(DEL_BTN_IN_CART).click()
    Alert(driver).accept()
    time.sleep(3)
    # Check if it has been removed
    assert "Balo Máy Tính Pofoko QAD15 Đen" not in driver.page_source

    # Test update item in cart
    itemCount = driver.find_element_by_css_selector("#item_36740")
    # Current we have 1 item in cart
    assert "1" in itemCount.get_property('value')
    total = driver.find_element_by_css_selector(TOTAL_COST)
    assert "699.000₫" in total.get_attribute("innerHTML")
    time.sleep(2)
    # Now we update amount of item from 1->2
    driver.find_element_by_css_selector(INCREMENT_BTN).click()
    driver.find_element_by_link_text("Cập nhật").click()
    total_2_item = total = driver.find_element_by_css_selector(TOTAL_COST)
    time.sleep(2)
    assert "1.398.000₫" in total_2_item.get_attribute("innerHTML")

  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
    unittest.main()
