from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import UnexpectedAlertPresentException
from selenium.common.exceptions import NoAlertPresentException

driver = webdriver.Firefox()
driver.get("https://pico.vn/36839/balo-may-tinh-pofoko-qad-15-mau-den.html")


driver.find_element_by_css_selector("a.action-button:nth-child(2)").click()
Alert(driver).accept()

assert "1" in driver.find_element_by_css_selector("#count_shopping_cart").get_attribute('innerHTML')
driver.get("https://pico.vn/cart")
item1 = driver.find_element_by_css_selector(".orderinfo-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)")
assert "Balo Máy Tính Pofoko QAD15 Đen" in item1.get_attribute("innerHTML")
driver.find_element_by_css_selector("a.button:nth-child(1)").click()

driver.get("https://pico.vn/36740/balo-dell-cao-cap--essential.html")
driver.find_element_by_css_selector("a.action-button:nth-child(2)").click()
Alert(driver).accept()
assert "2" in driver.find_element_by_css_selector("#count_shopping_cart").get_attribute('innerHTML')
driver.get("https://pico.vn/cart")
item2 = driver.find_element_by_css_selector(".orderinfo-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > a:nth-child(1)")
assert "Balo Dell Cao Cấp  Essential" in item2.get_attribute("innerHTML")
#double check
item1 = driver.find_element_by_css_selector(".orderinfo-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)")
assert "Balo Máy Tính Pofoko QAD15 Đen" in item1.get_attribute("innerHTML")

driver.find_element_by_css_selector(".orderinfo-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3) > p:nth-child(2) > a:nth-child(2)").click()
Alert(driver).accept()
assert "Balo Máy Tính Pofoko QAD15 Đen" not in item2.get_attribute("innerHTML")
itemCount = driver.find_element_by_css_selector("#item_36740")
assert "1" in itemCount.get_property('value')
time.sleep(2)
total = driver.find_element_by_css_selector(".orderinfo-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > strong:nth-child(1)")
assert "699.000₫" in total.get_attribute("innerHTML")
time.sleep(2)
driver.find_element_by_css_selector(".icon-up-dir").click()
driver.find_element_by_link_text("Cập nhật").click()
total = driver.find_element_by_css_selector(".orderinfo-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > strong:nth-child(1)")
assert "1.398.000₫" in total.get_attribute("innerHTML")